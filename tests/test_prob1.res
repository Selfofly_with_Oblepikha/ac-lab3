[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            5
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            3,
            "data",
            "data",
            1
        ]
    },
    {
        "opcode": "data",
        "term": [
            4,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "ld",
        "term": [
            5,
            "ld",
            "absolute",
            3
        ]
    },
    {
        "opcode": "fdiv",
        "term": [
            6,
            "fdiv",
            "data",
            3
        ]
    },
    {
        "opcode": "jc",
        "term": [
            7,
            "jc",
            "absolute",
            9
        ]
    },
    {
        "opcode": "jmp",
        "term": [
            8,
            "jmp",
            "absolute",
            11
        ]
    },
    {
        "opcode": "fdiv",
        "term": [
            9,
            "fdiv",
            "data",
            5
        ]
    },
    {
        "opcode": "jc",
        "term": [
            10,
            "jc",
            "absolute",
            13
        ]
    },
    {
        "opcode": "add",
        "term": [
            11,
            "add",
            "absolute",
            4
        ]
    },
    {
        "opcode": "sv",
        "term": [
            12,
            "sv",
            "absolute",
            4
        ]
    },
    {
        "opcode": "ld",
        "term": [
            13,
            "ld",
            "absolute",
            3
        ]
    },
    {
        "opcode": "add",
        "term": [
            14,
            "add",
            "data",
            1
        ]
    },
    {
        "opcode": "sv",
        "term": [
            15,
            "sv",
            "absolute",
            3
        ]
    },
    {
        "opcode": "sub",
        "term": [
            16,
            "sub",
            "data",
            10
        ]
    },
    {
        "opcode": "je",
        "term": [
            17,
            "je",
            "absolute",
            19
        ]
    },
    {
        "opcode": "jmp",
        "term": [
            18,
            "jmp",
            "absolute",
            5
        ]
    },
    {
        "opcode": "ld",
        "term": [
            19,
            "ld",
            "absolute",
            4
        ]
    },
    {
        "opcode": "sv",
        "term": [
            20,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "halt",
        "term": [
            21,
            "halt",
            "data",
            0
        ]
    }
]
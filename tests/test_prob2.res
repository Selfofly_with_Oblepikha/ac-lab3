[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            7
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            3,
            "data",
            "data",
            1
        ]
    },
    {
        "opcode": "data",
        "term": [
            4,
            "data",
            "data",
            2
        ]
    },
    {
        "opcode": "data",
        "term": [
            5,
            "data",
            "data",
            2
        ]
    },
    {
        "opcode": "data",
        "term": [
            6,
            "data",
            "data",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            7,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sub",
        "term": [
            8,
            "sub",
            "data",
            4000000
        ]
    },
    {
        "opcode": "jge",
        "term": [
            9,
            "jge",
            "absolute",
            22
        ]
    },
    {
        "opcode": "ld",
        "term": [
            10,
            "ld",
            "absolute",
            3
        ]
    },
    {
        "opcode": "add",
        "term": [
            11,
            "add",
            "absolute",
            4
        ]
    },
    {
        "opcode": "sv",
        "term": [
            12,
            "sv",
            "absolute",
            5
        ]
    },
    {
        "opcode": "ld",
        "term": [
            13,
            "ld",
            "absolute",
            4
        ]
    },
    {
        "opcode": "sv",
        "term": [
            14,
            "sv",
            "absolute",
            3
        ]
    },
    {
        "opcode": "ld",
        "term": [
            15,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            16,
            "sv",
            "absolute",
            4
        ]
    },
    {
        "opcode": "fdiv",
        "term": [
            17,
            "fdiv",
            "data",
            2
        ]
    },
    {
        "opcode": "jc",
        "term": [
            18,
            "jc",
            "absolute",
            7
        ]
    },
    {
        "opcode": "add",
        "term": [
            19,
            "add",
            "absolute",
            6
        ]
    },
    {
        "opcode": "sv",
        "term": [
            20,
            "sv",
            "absolute",
            6
        ]
    },
    {
        "opcode": "jmp",
        "term": [
            21,
            "jmp",
            "absolute",
            7
        ]
    },
    {
        "opcode": "ld",
        "term": [
            22,
            "ld",
            "absolute",
            6
        ]
    },
    {
        "opcode": "sv",
        "term": [
            23,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "halt",
        "term": [
            24,
            "halt",
            "data",
            0
        ]
    }
]
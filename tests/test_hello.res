[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            13
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            3,
            "data",
            "data",
            72
        ]
    },
    {
        "opcode": "data",
        "term": [
            4,
            "data",
            "data",
            101
        ]
    },
    {
        "opcode": "data",
        "term": [
            5,
            "data",
            "data",
            108
        ]
    },
    {
        "opcode": "data",
        "term": [
            6,
            "data",
            "data",
            111
        ]
    },
    {
        "opcode": "data",
        "term": [
            7,
            "data",
            "data",
            44
        ]
    },
    {
        "opcode": "data",
        "term": [
            8,
            "data",
            "data",
            32
        ]
    },
    {
        "opcode": "data",
        "term": [
            9,
            "data",
            "data",
            87
        ]
    },
    {
        "opcode": "data",
        "term": [
            10,
            "data",
            "data",
            114
        ]
    },
    {
        "opcode": "data",
        "term": [
            11,
            "data",
            "data",
            100
        ]
    },
    {
        "opcode": "data",
        "term": [
            12,
            "data",
            "data",
            33
        ]
    },
    {
        "opcode": "ld",
        "term": [
            13,
            "ld",
            "absolute",
            3
        ]
    },
    {
        "opcode": "sv",
        "term": [
            14,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            15,
            "ld",
            "absolute",
            4
        ]
    },
    {
        "opcode": "sv",
        "term": [
            16,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            17,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            18,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            19,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            20,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            21,
            "ld",
            "absolute",
            6
        ]
    },
    {
        "opcode": "sv",
        "term": [
            22,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            23,
            "ld",
            "absolute",
            7
        ]
    },
    {
        "opcode": "sv",
        "term": [
            24,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            25,
            "ld",
            "absolute",
            8
        ]
    },
    {
        "opcode": "sv",
        "term": [
            26,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            27,
            "ld",
            "absolute",
            9
        ]
    },
    {
        "opcode": "sv",
        "term": [
            28,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            29,
            "ld",
            "absolute",
            6
        ]
    },
    {
        "opcode": "sv",
        "term": [
            30,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            31,
            "ld",
            "absolute",
            10
        ]
    },
    {
        "opcode": "sv",
        "term": [
            32,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            33,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            34,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            35,
            "ld",
            "absolute",
            11
        ]
    },
    {
        "opcode": "sv",
        "term": [
            36,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            37,
            "ld",
            "absolute",
            12
        ]
    },
    {
        "opcode": "sv",
        "term": [
            38,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "halt",
        "term": [
            39,
            "halt",
            "data",
            0
        ]
    }
]
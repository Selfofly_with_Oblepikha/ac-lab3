[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            5
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            3,
            "data",
            "data",
            2520
        ]
    },
    {
        "opcode": "data",
        "term": [
            4,
            "data",
            "data",
            13
        ]
    },
    {
        "opcode": "ld",
        "term": [
            5,
            "ld",
            "absolute",
            3
        ]
    },
    {
        "opcode": "mul",
        "term": [
            6,
            "mul",
            "data",
            11
        ]
    },
    {
        "opcode": "mul",
        "term": [
            7,
            "mul",
            "absolute",
            4
        ]
    },
    {
        "opcode": "mul",
        "term": [
            8,
            "mul",
            "data",
            2
        ]
    },
    {
        "opcode": "mul",
        "term": [
            9,
            "mul",
            "data",
            17
        ]
    },
    {
        "opcode": "mul",
        "term": [
            10,
            "mul",
            "data",
            19
        ]
    },
    {
        "opcode": "sv",
        "term": [
            11,
            "sv",
            "absolute",
            3
        ]
    },
    {
        "opcode": "sv",
        "term": [
            12,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "halt",
        "term": [
            13,
            "halt",
            "data",
            0
        ]
    }
]
[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            3
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "ld",
        "term": [
            3,
            "ld",
            "absolute",
            1
        ]
    },
    {
        "opcode": "je",
        "term": [
            4,
            "je",
            "absolute",
            7
        ]
    },
    {
        "opcode": "sv",
        "term": [
            5,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "jmp",
        "term": [
            6,
            "jmp",
            "absolute",
            3
        ]
    },
    {
        "opcode": "halt",
        "term": [
            7,
            "halt",
            "data",
            0
        ]
    }
]
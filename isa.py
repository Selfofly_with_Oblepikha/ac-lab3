"""Модуль, содержащий информацию об инструкциях"""
import json
from collections import namedtuple
from enum import Enum


class Opcode(str, Enum):
    """Коды операций"""

    DATA = 'data'

    LD = 'ld'
    SV = 'sv'

    ADD = 'add'
    SUB = 'sub'
    MUL = 'mul'
    DIV = 'div'

    FADD = 'fadd'
    FSUB = 'fsub'
    FMUL = 'fmul'
    FDIV = 'fdiv'

    JMP = 'jmp'
    JE = 'je'
    JGE = 'jge'
    JC = 'jc'

    HLT = 'halt'


class AddrMode(str, Enum):
    """Режимы адресации"""
    ABS = 'absolute'
    DATA = 'data'


class Term(namedtuple('Term', 'line operation mode argument')):
    """Полное описание инструкции."""


def write_code(filename, code):
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename):
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code:
        instr['opcode'] = Opcode(instr['opcode'])
        if 'term' in instr:
            instr['term'] = Term(instr['term'][0], instr['term'][1],
                                 AddrMode(instr['term'][2]), instr['term'][3])

    return code

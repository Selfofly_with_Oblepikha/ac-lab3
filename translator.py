#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=invalid-name                # сохраним традиционные наименования сигналов
# pylint: disable=consider-using-f-string     # избыточный синтаксис
"""Модуль трансляции исходного кода в машинный"""
import sys

from isa import Opcode, AddrMode, write_code, Term

symbol2opcode = {
    "data": Opcode.DATA,
    "ld": Opcode.LD,
    "sv": Opcode.SV,
    "add": Opcode.ADD,
    "sub": Opcode.SUB,
    "mul": Opcode.MUL,
    "div": Opcode.DIV,
    "fadd": Opcode.FADD,
    "fsub": Opcode.FSUB,
    "fmul": Opcode.FMUL,
    "fdiv": Opcode.FDIV,
    "jmp": Opcode.JMP,
    "je": Opcode.JE,
    "jge": Opcode.JGE,
    "jc": Opcode.JC,
    "halt": Opcode.HLT
}


def translate(text):
    cell_counter = 0
    terms = []
    labels = {}
    variables = {}
    no_mod_list = []

    is_data = True
    # Добавим в начало программы функцию перепрыгивания области данных
    terms.append(Term(cell_counter, 'jmp', AddrMode.ABS, '.start'))
    cell_counter += 1

    # Выделим ячейки для ввода/вывода и создадим под них переменные
    # Так их будет удобнее использовать из кода
    # Так же мы не хотим, чтобы эти ячейки случайно модифицировались транслятором дальше
    # Поэтому добавим их в no_mod_list
    terms.append(Term(cell_counter, 'data', AddrMode.DATA, 0))  # INPUT 1
    variables['INPUT'] = cell_counter
    no_mod_list.append(cell_counter)
    cell_counter += 1
    terms.append(Term(cell_counter, 'data', AddrMode.DATA, 0))  # OUTPUT 2
    variables['OUTPUT'] = cell_counter
    no_mod_list.append(cell_counter)
    cell_counter += 1

    for line in text.split("\n"):
        words = line.split()
        if len(words) == 0:
            continue
        # Отсеем все комментарии
        if words[0][0] == ';':
            continue
        for i in range(len(words)):
            words[i] = str(words[i])
            if words[i][0] == ';':
                words = words[0:i - 1]
                break

        assert len(words) < 3, "Unknown construction: too much parameters!"
        # Обработаем метки
        if len(words) == 1 and words[0][0] == '.' and words[0][len(words[0]) - 1] == ':':
            assert words[0][0:len(words[0]) - 1] not in labels.keys(), "Duplicating labels!"
            labels[words[0][0:len(words[0]) - 1]] = cell_counter
            # Если встретили метку .data, значит дальше лежат данные. Иначе - код
            is_data = bool(words[0] == '.data:')
            continue

        # Данные и код обрабатываем отдельно
        if is_data:
            assert len(words) == 2, "Wrong data format!"
            assert words[0] not in variables.keys(), "Duplicating variables!"
            variables[words[0]] = cell_counter
            if len(words[1]) == 3 and words[1][0] == '\'' and words[1][2] == '\'':
                terms.append(Term(cell_counter, 'data', AddrMode.DATA, ord(words[1][1])))
            else:
                terms.append(Term(cell_counter, 'data', AddrMode.DATA, words[1]))
        else:
            assert words[0] in symbol2opcode.keys(), "Unknown command!"
            if len(words) == 1:
                words.append(0)
            terms.append(Term(cell_counter, words[0], AddrMode.DATA, words[1]))

        cell_counter += 1

    assert '.start' in labels.keys(), "No starting label found!"

    code = []

    # Разворачиваем метки и переменные и сразу записываем в результирующий JSON-список
    for i in range(len(terms)):
        if terms[i].line not in no_mod_list:
            if terms[i].argument in labels.keys():
                terms[i] = Term(terms[i].line, terms[i].operation,
                                AddrMode.ABS, labels[terms[i].argument])
            elif terms[i].argument in variables.keys():
                terms[i] = Term(terms[i].line, terms[i].operation,
                                AddrMode.ABS, variables[terms[i].argument])
            else:
                terms[i] = Term(terms[i].line, terms[i].operation,
                                AddrMode.DATA, int(terms[i].argument))
        code.append({'opcode': symbol2opcode[terms[i].operation], 'term': terms[i]})

    return code


def main(args):
    assert len(args) == 2, "Wrong arguments: translator.py <input_code_file> <target_file>"

    source, target = args

    with open(source, "rt", encoding="utf-8") as f:
        source = f.read()

    code = translate(source)
    print("source LoC:", len(source.split()), "code instr:", len(code))
    write_code(target, code)


if __name__ == '__main__':
    main(sys.argv[1:])

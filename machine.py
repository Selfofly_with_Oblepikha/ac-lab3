#!/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью
# pylint: disable=invalid-name                # сохраним традиционные наименования сигналов
# pylint: disable=consider-using-f-string     # избыточный синтаксис
"""Модуль, эмулирующий процессор"""

import logging
import sys
from enum import Enum

from isa import Opcode, AddrMode, read_code, Term


class AccLatchSignals(Enum):
    """Список управляющих сигналов для latch_acc"""
    ALU = 0
    ARG = 1
    MEM = 2
    SELF = 3


class AluOperations(Enum):
    """Список операций АЛУ"""
    ADD = 0
    SUB = 1
    MUL = 2
    DIV = 3


opcode2operation = {
    Opcode.ADD: AluOperations.ADD,
    Opcode.FADD: AluOperations.ADD,
    Opcode.SUB: AluOperations.SUB,
    Opcode.FSUB: AluOperations.SUB,
    Opcode.MUL: AluOperations.MUL,
    Opcode.FMUL: AluOperations.MUL,
    Opcode.DIV: AluOperations.DIV,
    Opcode.FDIV: AluOperations.DIV
}


class ControlUnit:
    """Класс, представляющий собой модель самого процессора"""
    def __init__(self, memory, input_buffer):
        self.memory = memory
        self.input_buffer = input_buffer
        self.input_counter = 0
        self.output_buffer = []
        self._tick = 0
        self.acc = 0
        self.addr = 0
        self.ip = 0
        self.step_counter = 0
        self.z_flag = 0
        self.c_flag = 0
        self.p_flag = 0

    def tick(self):
        self._tick += 1

    def current_tick(self):
        return self._tick

    def latch_acc(self, sel_acc, operation=AluOperations.ADD, sel_data=True):
        res = 0
        if sel_acc == AccLatchSignals.ALU:
            res = self.alu_calculate(operation, sel_data)
        elif sel_acc == AccLatchSignals.ARG:
            res = self.memory[self.ip]['term'].argument
        elif sel_acc == AccLatchSignals.MEM:
            if self.addr == 1:  # INPUT
                res = self.input_buffer[self.input_counter]
                self.input_counter += 1
            else:
                res = self.memory[self.addr]['term'].argument
        elif sel_acc == AccLatchSignals.SELF:
            self.alu_calculate(operation, sel_data)
            res = self.acc
        # Силой пихаем в 64 бита
        while res > 9223372036854775807:
            res = -9223372036854775808 + (res - 9223372036854775807)
        while res < -9223372036854775808:
            res = 9223372036854775807 - (res + 9223372036854775808)
        self.acc = res
        if self.acc == 0:
            self.z_flag = 1
        else:
            self.z_flag = 0
        if self.acc > 0:
            self.p_flag = 1
        else:
            self.p_flag = 0

    def latch_addr(self):
        res = self.memory[self.ip]['term'].argument
        # Силой пихаем в 64 бита
        while res > 9223372036854775807:
            res = -9223372036854775808 + (res - 9223372036854775807)
        while res < -9223372036854775808:
            res = 9223372036854775807 - (res + 9223372036854775808)
        self.addr = res

    def latch_step_counter(self, sel_next):
        if sel_next:
            self.step_counter += 1
        else:
            self.step_counter = 0

    def latch_ip(self, sel_next):
        if sel_next:
            res = self.ip + 1
        else:
            res = self.memory[self.ip]['term'].argument
        # Силой пихаем в 64 бита (даже этот регистр пусть будет 64-битный)
        while res > 9223372036854775807:
            res = -9223372036854775808 + (res - 9223372036854775807)
        while res < -9223372036854775808:
            res = 9223372036854775807 - (res + 9223372036854775808)
        self.ip = res

    def latch_mem(self):
        if self.addr == 2:  # OUTPUT
            self.output_buffer.append(self.acc)
        else:
            data = self.memory[self.addr]
            self.memory[self.addr]['term'] = \
                Term(data['term'].line, data['term'].operation, data['term'].mode, self.acc)

    def alu_calculate(self, operation, sel_data):
        if sel_data:
            second_operand = self.memory[self.ip]['term'].argument
        else:
            second_operand = self.memory[self.addr]['term'].argument
        # Силой пихаем в 64 бита
        while second_operand > 9223372036854775807:
            second_operand = -9223372036854775808 + (second_operand - 9223372036854775807)
        while second_operand < -9223372036854775808:
            second_operand = 9223372036854775807 - (second_operand + 9223372036854775808)
        res = 0
        if operation == AluOperations.ADD:
            res = self.acc + second_operand
            self.c_flag = 0
            if res > 9223372036854775807:
                res = -9223372036854775808 + (res - 9223372036854775807)
                self.c_flag = 1
        elif operation == AluOperations.SUB:
            res = self.acc - second_operand
            self.c_flag = 0
            if res < -9223372036854775808:
                res = 9223372036854775807 - (res + 9223372036854775808)
                self.c_flag = 1
        elif operation == AluOperations.MUL:
            res = self.acc * second_operand
            self.c_flag = 0
            while res > 9223372036854775807:
                res = -9223372036854775808 + (res - 9223372036854775807)
                self.c_flag = 1
        elif operation == AluOperations.DIV:
            res = self.acc / second_operand
            if self.acc % second_operand != 0:
                self.c_flag = 1
            else:
                self.c_flag = 0
        return res

    def decode_and_execute_instruction(self):
        instr = self.memory[self.ip]
        opcode = instr["opcode"]
        self.tick()

        if opcode is Opcode.HLT:
            raise StopIteration()

        if opcode in {Opcode.JMP, Opcode.JE, Opcode.JGE, Opcode.JC}:
            sel_next = False
            if opcode == Opcode.JE and self.z_flag == 0:
                sel_next = True
            elif opcode == Opcode.JGE and not (self.z_flag == 1 or self.p_flag == 1):
                sel_next = True
            elif opcode == Opcode.JC and self.c_flag == 0:
                sel_next = True
            self.latch_ip(sel_next=sel_next)

        if opcode is Opcode.LD:
            if instr['term'].mode == AddrMode.ABS:
                if self.step_counter == 0:
                    self.latch_addr()
                    self.latch_step_counter(sel_next=True)
                elif self.step_counter == 1:
                    self.latch_acc(sel_acc=AccLatchSignals.MEM)
                    self.latch_step_counter(sel_next=False)
                    self.latch_ip(sel_next=True)
            else:
                self.latch_acc(sel_acc=AccLatchSignals.ARG)
                self.latch_ip(sel_next=True)

        if opcode is Opcode.SV:
            if self.step_counter == 0:
                self.latch_addr()
                self.latch_step_counter(sel_next=True)
            elif self.step_counter == 1:
                self.latch_mem()
                self.latch_step_counter(sel_next=False)
                self.latch_ip(sel_next=True)

        if opcode in {Opcode.ADD, Opcode.FADD, Opcode.SUB, Opcode.FSUB,
                      Opcode.MUL, Opcode.FMUL, Opcode.DIV, Opcode.FDIV}:
            sel_acc = AccLatchSignals.ALU
            if opcode in {Opcode.FADD, Opcode.FDIV, Opcode.FMUL, Opcode.FSUB}:
                sel_acc = AccLatchSignals.SELF

            if instr['term'].mode == AddrMode.ABS:
                if self.step_counter == 0:
                    self.latch_addr()
                    self.latch_step_counter(sel_next=True)
                elif self.step_counter == 1:
                    self.latch_acc(sel_acc=sel_acc,
                                   operation=opcode2operation[opcode],
                                   sel_data=False)
                    self.latch_step_counter(sel_next=False)
                    self.latch_ip(sel_next=True)
            elif instr['term'].mode == AddrMode.DATA:
                self.latch_acc(sel_acc=sel_acc,
                               operation=opcode2operation[opcode], sel_data=True)
                self.latch_ip(sel_next=True)

    def __repr__(self):
        state = "{{TICK: {}, IP: {}, ADDR: {}, ACC: {}, Z: {}, C: {}, P: {}, ST_C: {}}}".format(
            self._tick,
            self.ip,
            self.addr,
            self.acc,
            self.z_flag,
            self.c_flag,
            self.p_flag,
            self.step_counter
        )

        instr = self.memory[self.ip]
        opcode = instr["opcode"]
        arg = instr["term"].argument
        mode = instr["term"].mode
        cell_num = instr["term"].line
        action = "{} {} {} {}".format(cell_num, opcode, mode, arg)

        return "{} {}".format(state, action)


def simulation(code, limit, input_buffer):
    control_unit = ControlUnit(code, input_buffer)
    instr_counter = 0

    logging.debug('%s', control_unit)
    try:
        while True:
            assert limit > instr_counter, "too long execution, increase limit!"
            control_unit.decode_and_execute_instruction()
            if control_unit.step_counter == 0:
                instr_counter += 1
            logging.debug('%s', control_unit)
    except StopIteration:
        pass
    output = ''
    for c in control_unit.output_buffer:
        output += str(c)
        if c in range(0x110000):
            output += '(' + chr(c) + ')'
    logging.info('output: %s', output)
    return output, instr_counter, control_unit.current_tick()


def main(args):
    assert len(args) == 2, "Wrong arguments: machine.py <code_file> <input_file>"
    code_file, input_file = args

    input_token = []
    memory = read_code(code_file)
    with open(input_file, encoding="utf-8") as file:
        input_text = file.read()
        for char in input_text:
            input_token.append(ord(char))

    input_token.append(0)
    cell_counter = len(memory)

    # "Доаллоцируем" память - пусть будет 512 ячеек (при желании можно увеличить)
    num = 512
    assert cell_counter < num, "Not enough memory!"
    while cell_counter < num:
        memory.append({'opcode': Opcode.DATA,
                      'term': Term(cell_counter, 'data', AddrMode.DATA, '0')})
        cell_counter += 1

    output, instr_counter, ticks = simulation(memory, 1000, input_token)

    print("Output:")
    print(output)
    print("instr_counter:", instr_counter, "ticks:", ticks)
    return output


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])

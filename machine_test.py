"""
Unit-тесты для процессора
"""
import unittest
import machine
import translator


class MachineTest(unittest.TestCase):
    """
    Unit-тесты для процессора
    """
    input = "tests/input.txt"

    def start_machine(self, code, output):
        translator.main([code, output])
        return machine.main([output, self.input])

    def test_prob2(self):
        output = self.start_machine("tests/test_prob2", "tests/test_prob2.res")
        self.assertEqual(output, '4613732')

    def test_cat(self):
        output = self.start_machine("tests/test_cat", "tests/test_cat.res")
        self.assertEqual(output, '72(H)97(a)118(v)101(e)'
                                 '32( )97(a)32( )103(g)114(r)101(e)97(a)116(t)'
                                 '32( )100(d)97(a)121(y)33(!)')

    def test_hello(self):
        output = self.start_machine("tests/test_hello", "tests/test_hello.res")
        self.assertEqual(output, '72(H)101(e)108(l)108(l)111(o)44(,)'
                                 '32( )87(W)111(o)114(r)108(l)100(d)33(!)')

    def test_prob1(self):
        output = self.start_machine("tests/test_prob1", "tests/test_prob1.res")
        self.assertEqual(output, '23()')

    def test_prob5(self):
        output = self.start_machine("tests/test_prob5", "tests/test_prob5.res")
        self.assertEqual(output, '232792560')

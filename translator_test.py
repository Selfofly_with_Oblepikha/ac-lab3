"""
Unit-тесты для транслятора
"""

import unittest
import translator
import isa


class TranslatorTest(unittest.TestCase):
    """Unit-тесты для транслятора"""

    def simple_test(self, input_file, output, correct):
        translator.main([input_file, output])

        result_code = isa.read_code(output)
        correct_code = isa.read_code(correct)

        self.assertEqual(result_code, correct_code)

    def test_cat(self):
        self.simple_test("tests/test_cat", "tests/test_cat.res",
                         "tests/test_cat_correct.res")

    def test_prob2(self):
        self.simple_test("tests/test_prob2", "tests/test_prob2.res", "tests/test_prob2_correct.res")

    def test_hello_world(self):
        self.simple_test("tests/test_hello", "tests/test_hello.res",
                         "tests/test_hello_correct.res")

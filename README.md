# ASM-like. Транслятор и модель

- Бойко Владислава Алексеевича, студента факультета ПИиКТ. 312421.
- `asm | acc | neum | mc | tick | struct | stream | mem | prob2`


## Язык программирования

- Поддерживаются комментарии: начиная с символа ";" и до конца строки все считается комментарием. Пример:
```
. . .
add 2 ; <-- Here starts the comment - code will execute
; This is comment too - no code
; add 2 <-- This code won't execute - it is commented
. . .
```

- Поддерживаются метки: если в строке ровно 1 слово (не считая комментариев), оно начинается с "." и заканчивается ":" - это считается меткой. (Все метки должны иметь уникальные имена!) Пример:
```
. . .
.start: ; This is .start label - here the program starts
        ; By the way, tabulation doesn't matter, use it as you like
    add 2
    sub 20
    jmp .start ; You can jump to it!
    . . .
```

- Управляющие метки:
    - `.data` - метка, сигнализирующая о начале области данных. После нее можно объявлять переменные, но нельзя писать код. Любая другая метка подразумевает под собой начало области кода. (По дефолту, пока не встретилась любая другая метка, все, что вы пишете в файле, расценивается как область данных - валидируется соответствующим образом - об этом ниже. Таким образом, можно метку .data и не использовать.)
    - `.start` - ОБЯЗАТЕЛЬНАЯ метка, сигнализирующая транслятору, что с этого места начинается исполнение программы.


- Поддерживаются переменные: любая строка в области данных должна состоять из 2 слов - имени переменной и значения. (Все переменные должны иметь уникальные имена!) Значением может быть:
    - Число. В таком случае следует просто написать его без каких-либо специальных символов.
    - Char. В таком случае следует записать его в одинарных кавычках. Любые символы будут сразу же странслированы в числа.
Пример:
```
. . .
.data:
    my_int 1488
    my_char 'G'
    my_else 'Georgia' ; WRONG! Nothing else is supported!!!
. . .
```

- Существуют 2 автоматические переменные, отвечающие за ввод/вывод: `INPUT` и `OUTPUT`. Пример использования:
```
. . .
.text:
    .start:
        ld INPUT  ; Read from standart input
        sv OUTPUT ; Write to standart output
. . .
```

Код выполняется последовательно. Операции:
- `ld` -- загрузить значение в аккумулятор из памяти
- `sv` -- выгрузить значение из аккумулятора в память
- `add` -- прибавить к значению аккумулятора указанное
- `sub` -- вычесть из значения аккумулятора указанное
- `mul` -- помножить значение аккумулятора на указанное
- `div` -- поделить значение аккумулятора на указанное
- `fadd/fsub/fmul/fdiv` -- те же операции, что выше, но не сохраняют значение в аккумулятор - только выставляют флаг C (carry)
- `jmp` -- безусловный переход в указанное место в программе
- `je` -- переход в случае, если значение аккумулятора - 0
- `jge` -- переход в случае, если значение аккумулятора >= 0
- `jc` -- переход в случае, если выставлен флаг C
- `halt` -- точка останова

## Система команд

Особенности процессора:

- Машинное слово -- 64 бит, знаковое.
- Регистры:
    - `acc` - accumulator - аккумуляторный регистр, вокруг которого построена система команд.
    - `ip` - instruction pointer - регистр, аддресующий в памяти исполняющуюся инструкцию. Стандартное поведение - увеличение на 1 после каждой инструкции, но можно манипулировать вручную с помощью команд перехода.
    - `addr` - address register - адресный регистр, адресующий данные в памяти. Необходим для режимов абсолютной и относительной аддресации (поддерживаются скрытно).
    - `step_counter` - отвечает за подсчет тактов в процессе исполнения многотактовых инструкций.
    - `z_flag, c_flag, p_flag` - флаговые регистры. Z - выставлен в случае, когда в аккумуляторе лежит 0. P - выставлен, когда в аккумуляторе лежит положительное число. C - выставлен, когда результат операции либо не поместился в машинное слово, либо не является целым числом.


- Ввод-вывод -- мапится на память (автоматически создаются переменные INPUT и OUTPUT, запись/чтение из которых отлавливаются и перенаправляются в соответствующие буфферы).
- Поддерживаемые режимы аддресации:
    - DATA - без аддресации, аргумент инструкции - операнд
    - ABS - абсолютная аддресация, аргумент инструкции - адрес, по которому лежит операнд

    Все режимы поддерживаются скрытно - пользователь о них не знает, они нужны только "за кадром"


### Набор инструкций

Обратите внимание - в качестве аргумента любой инструкции могут быть переданы:
- Число - остается числом
- Переменная - на этапе трансляции вместо нее подставляется адрес в памяти, где она будет расположена
- Метка - на этапе трансляции вместо нее подставляется адрес следующей ячейки в памяти (будь там переменная или инструкция)

Ответственность за осмысленность применения того или иного варианта в каждой ситуации возложена на программиста!

| Syntax                 | Mnemonic              | Кол-во тактов | Comment                          |
|:-----------------------|:----------------------|---------------|:---------------------------------|
| `ld <val>`             | ld `<val>`            | 2/1           | количество тактов зависит от режима аддресации |
| `sv <val>`             | sv `<val>`            | 2             | количество тактов зависит от режима аддресации |
| `add/fadd <val>`       | add/fadd `<val>`      | 2/1           | количество тактов зависит от режима аддресации |
| `sub/fsub <val>`       | sub/fsub `<val>`      | 2/1           | количество тактов зависит от режима аддресации |
| `mul/fmul <val>`       | mul/fmul `<val>`      | 2/1           | количество тактов зависит от режима аддресации |
| `div/fdiv <val>`       | div/fdiv `<val>`      | 2/1           | количество тактов зависит от режима аддресации |
| `jmp/je/jge/jc <val>`  | jmp/je/jge/jc `<val>` | 1             | количество тактов зависит от режима аддресации |
| `halt`                 | halt                  | 0             | см. язык                         |


### Кодирование инструкций

- Машинный код сериализуется в список JSON.
- Один элемент списка, одна инструкция.
- Индекс списка -- адрес инструкции. Используется для команд перехода.

Пример:

```json
[
    {
        "opcode": "sv",
        "term": [
            5,
            "sv",
            "absolute",
            1
        ]
    }
]
```

где:

- `opcode` -- строка с кодом операции;
- `term` -- полная информация об инструкции (номер, код, режим аддресации, аргумент)

Типы данные в модуле [isa](./isa.py), где:

- `Opcode` -- перечисление кодов операций;
- `AddrMode` -- перечисление режимор аддресации;
- `Term` -- структура для описания всей необходимой информации об инструкции.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>"`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):

1. Трансформирование текста программы в последовательность термов (включает в себя отсеивание комментариев, обработку меток и переменных, валидацию формата ввода)
2. Проверка наличия метки `.start`
3. Генерация машинного кода

Правила генерации машинного кода:

- один терм -- одна инструкция;
- вместо переменных подставляются их адреса, вместо меток - адреса следующих ячеек в памяти

## Модель процессора

Реализовано в модуле: [machine](./machine.py).

Мной было принято решение не разделять DataPath и ControlUnit ввиду личного удобства, а реализовать модель в рамках одного структурного элемента.

![](./model.jpg)

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `latch_acc` -- защёлкнуть аккумулятор. Принимает управляющий сигнал, указывающий на источник данных. Автоматически защелкивает Z и P флаги. Если источником указан АЛУ, необходимо так же передать 3 дополнительных управляющих сигнала: один отвечает за код операции, один - за источник данных для второго операнда, и еще один за то, стоит ли сохранить результат вычислений, или заменить его текущим значением аккумулятора;
- `latch_addr` -- защёлкнуть аддресный регистр. Принимает управляющий сигнал, указывающий на источник данных;
- `latch_step_counter` --  защелкнуть счетчик шагов исполнения инструкции. Принимает управляющий сигнал, указывающий на источник данных;
- `latch_ip` -- защелкнуть instruction pointer. Принимает управляющий сигнал, указывающий на источник данных;
- `latch_mem` -- выгрузить данные из аккумулятора в память;

Флаги:
- Z - zero
- P - positive
- C - carry

Моделирование производилось на уровне микрокоманд.
Трансляция инструкций в последовательность сигналов - decode_and_execute_instruction.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль `logging`.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключения:
    - `StopIteration` -- если выполнена инструкция halt.
- Управление симуляцией реализовано в функции simulate.

## Апробация

Тесты поделены на 2 группы - тесты для транслятора и тесты для симулятора. Начнем с [первых](./translator_test.py):

1. [hello world](./tests/test_hello).
2. [cat](./tests/test_cat) -- программа `cat`, повторяем ввод на выводе.
3. [prob2](./tests/test_prob2) -- 2 проблема Эйлера из моего варианта.

Пример для `hello world`:

```
(base) selfofly@selfofly-with-oblepikha:~/Documents/Labs/ACLabs/ac-lab3$ cat tests/test_hello
.data:
	H 72
	e 101
	l 108
	o 111
	comma 44
	space 32
	W 87
	r 114
	d 100
	! 33
.text:
	.start:
        	ld H
        	sv OUTPUT
        	ld e
        	sv OUTPUT
        	ld l
        	sv OUTPUT
        	ld l
        	sv OUTPUT
        	ld o
        	sv OUTPUT
        	ld comma
        	sv OUTPUT
        	ld space
        	sv OUTPUT
        	ld W
        	sv OUTPUT
        	ld o
        	sv OUTPUT
        	ld r
        	sv OUTPUT
        	ld l
        	sv OUTPUT
        	ld d
        	sv OUTPUT
        	ld !
        	sv OUTPUT
	.hlt:
		halt
(base) selfofly@selfofly-with-oblepikha:~/Documents/Labs/ACLabs/ac-lab3$ python translator.py tests/test_hello tests/test_hello.res
source LoC: 77 code instr: 40
(base) selfofly@selfofly-with-oblepikha:~/Documents/Labs/ACLabs/ac-lab3$ cat tests/test_hello.res
[
    {
        "opcode": "jmp",
        "term": [
            0,
            "jmp",
            "absolute",
            13
        ]
    },
    {
        "opcode": "data",
        "term": [
            1,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            2,
            "data",
            "data",
            0
        ]
    },
    {
        "opcode": "data",
        "term": [
            3,
            "data",
            "data",
            72
        ]
    },
    {
        "opcode": "data",
        "term": [
            4,
            "data",
            "data",
            101
        ]
    },
    {
        "opcode": "data",
        "term": [
            5,
            "data",
            "data",
            108
        ]
    },
    {
        "opcode": "data",
        "term": [
            6,
            "data",
            "data",
            111
        ]
    },
    {
        "opcode": "data",
        "term": [
            7,
            "data",
            "data",
            44
        ]
    },
    {
        "opcode": "data",
        "term": [
            8,
            "data",
            "data",
            32
        ]
    },
    {
        "opcode": "data",
        "term": [
            9,
            "data",
            "data",
            87
        ]
    },
    {
        "opcode": "data",
        "term": [
            10,
            "data",
            "data",
            114
        ]
    },
    {
        "opcode": "data",
        "term": [
            11,
            "data",
            "data",
            100
        ]
    },
    {
        "opcode": "data",
        "term": [
            12,
            "data",
            "data",
            33
        ]
    },
    {
        "opcode": "ld",
        "term": [
            13,
            "ld",
            "absolute",
            3
        ]
    },
    {
        "opcode": "sv",
        "term": [
            14,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            15,
            "ld",
            "absolute",
            4
        ]
    },
    {
        "opcode": "sv",
        "term": [
            16,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            17,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            18,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            19,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            20,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            21,
            "ld",
            "absolute",
            6
        ]
    },
    {
        "opcode": "sv",
        "term": [
            22,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            23,
            "ld",
            "absolute",
            7
        ]
    },
    {
        "opcode": "sv",
        "term": [
            24,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            25,
            "ld",
            "absolute",
            8
        ]
    },
    {
        "opcode": "sv",
        "term": [
            26,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            27,
            "ld",
            "absolute",
            9
        ]
    },
    {
        "opcode": "sv",
        "term": [
            28,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            29,
            "ld",
            "absolute",
            6
        ]
    },
    {
        "opcode": "sv",
        "term": [
            30,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            31,
            "ld",
            "absolute",
            10
        ]
    },
    {
        "opcode": "sv",
        "term": [
            32,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            33,
            "ld",
            "absolute",
            5
        ]
    },
    {
        "opcode": "sv",
        "term": [
            34,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            35,
            "ld",
            "absolute",
            11
        ]
    },
    {
        "opcode": "sv",
        "term": [
            36,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "ld",
        "term": [
            37,
            "ld",
            "absolute",
            12
        ]
    },
    {
        "opcode": "sv",
        "term": [
            38,
            "sv",
            "absolute",
            2
        ]
    },
    {
        "opcode": "halt",
        "term": [
            39,
            "halt",
            "data",
            0
        ]
    }
]

```


Не буду показывать вывод для cat и prob2 чтобы не загромождать readme - с желаемым результатом для них можно ознакомиться в соответствующих файлах в папке tests.

Тесты, покрывающие [симулятор](./machine_test.py):

1. [hello world](./tests/test_hello).
2. [cat](./tests/test_cat) -- программа `cat`, повторяем ввод на выводе.
3. [prob1](./tests/test_prob1) -- 1 проблема Эйлера средствами, доступными в моей реализации.
4. [prob2](./tests/test_prob2) -- 2 проблема Эйлера из моего варианта.
5. [prob1](./tests/test_prob5) -- упрощенная реализация 5 проблемы Эйлера средствами, доступными в моей реализации.

Пример для `hello world`, продолжающий предыдущий:

```
(base) selfofly@selfofly-with-oblepikha:~/Documents/Labs/ACLabs/ac-lab3$ python machine.py tests/test_hello.res tests/input.txtDEBUG:root:{TICK: 0, IP: 0, ADDR: 0, ACC: 0, Z: 0, C: 0, P: 0, ST_C: 0} 0 jmp absolute 13
DEBUG:root:{TICK: 1, IP: 13, ADDR: 0, ACC: 0, Z: 0, C: 0, P: 0, ST_C: 0} 13 ld absolute 3
DEBUG:root:{TICK: 2, IP: 13, ADDR: 3, ACC: 0, Z: 0, C: 0, P: 0, ST_C: 1} 13 ld absolute 3
DEBUG:root:{TICK: 3, IP: 14, ADDR: 3, ACC: 72, Z: 0, C: 0, P: 1, ST_C: 0} 14 sv absolute 2
DEBUG:root:{TICK: 4, IP: 14, ADDR: 2, ACC: 72, Z: 0, C: 0, P: 1, ST_C: 1} 14 sv absolute 2
DEBUG:root:{TICK: 5, IP: 15, ADDR: 2, ACC: 72, Z: 0, C: 0, P: 1, ST_C: 0} 15 ld absolute 4
DEBUG:root:{TICK: 6, IP: 15, ADDR: 4, ACC: 72, Z: 0, C: 0, P: 1, ST_C: 1} 15 ld absolute 4
DEBUG:root:{TICK: 7, IP: 16, ADDR: 4, ACC: 101, Z: 0, C: 0, P: 1, ST_C: 0} 16 sv absolute 2
DEBUG:root:{TICK: 8, IP: 16, ADDR: 2, ACC: 101, Z: 0, C: 0, P: 1, ST_C: 1} 16 sv absolute 2
DEBUG:root:{TICK: 9, IP: 17, ADDR: 2, ACC: 101, Z: 0, C: 0, P: 1, ST_C: 0} 17 ld absolute 5
DEBUG:root:{TICK: 10, IP: 17, ADDR: 5, ACC: 101, Z: 0, C: 0, P: 1, ST_C: 1} 17 ld absolute 5
DEBUG:root:{TICK: 11, IP: 18, ADDR: 5, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 0} 18 sv absolute 2
DEBUG:root:{TICK: 12, IP: 18, ADDR: 2, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 1} 18 sv absolute 2
DEBUG:root:{TICK: 13, IP: 19, ADDR: 2, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 0} 19 ld absolute 5
DEBUG:root:{TICK: 14, IP: 19, ADDR: 5, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 1} 19 ld absolute 5
DEBUG:root:{TICK: 15, IP: 20, ADDR: 5, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 0} 20 sv absolute 2
DEBUG:root:{TICK: 16, IP: 20, ADDR: 2, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 1} 20 sv absolute 2
DEBUG:root:{TICK: 17, IP: 21, ADDR: 2, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 0} 21 ld absolute 6
DEBUG:root:{TICK: 18, IP: 21, ADDR: 6, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 1} 21 ld absolute 6
DEBUG:root:{TICK: 19, IP: 22, ADDR: 6, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 0} 22 sv absolute 2
DEBUG:root:{TICK: 20, IP: 22, ADDR: 2, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 1} 22 sv absolute 2
DEBUG:root:{TICK: 21, IP: 23, ADDR: 2, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 0} 23 ld absolute 7
DEBUG:root:{TICK: 22, IP: 23, ADDR: 7, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 1} 23 ld absolute 7
DEBUG:root:{TICK: 23, IP: 24, ADDR: 7, ACC: 44, Z: 0, C: 0, P: 1, ST_C: 0} 24 sv absolute 2
DEBUG:root:{TICK: 24, IP: 24, ADDR: 2, ACC: 44, Z: 0, C: 0, P: 1, ST_C: 1} 24 sv absolute 2
DEBUG:root:{TICK: 25, IP: 25, ADDR: 2, ACC: 44, Z: 0, C: 0, P: 1, ST_C: 0} 25 ld absolute 8
DEBUG:root:{TICK: 26, IP: 25, ADDR: 8, ACC: 44, Z: 0, C: 0, P: 1, ST_C: 1} 25 ld absolute 8
DEBUG:root:{TICK: 27, IP: 26, ADDR: 8, ACC: 32, Z: 0, C: 0, P: 1, ST_C: 0} 26 sv absolute 2
DEBUG:root:{TICK: 28, IP: 26, ADDR: 2, ACC: 32, Z: 0, C: 0, P: 1, ST_C: 1} 26 sv absolute 2
DEBUG:root:{TICK: 29, IP: 27, ADDR: 2, ACC: 32, Z: 0, C: 0, P: 1, ST_C: 0} 27 ld absolute 9
DEBUG:root:{TICK: 30, IP: 27, ADDR: 9, ACC: 32, Z: 0, C: 0, P: 1, ST_C: 1} 27 ld absolute 9
DEBUG:root:{TICK: 31, IP: 28, ADDR: 9, ACC: 87, Z: 0, C: 0, P: 1, ST_C: 0} 28 sv absolute 2
DEBUG:root:{TICK: 32, IP: 28, ADDR: 2, ACC: 87, Z: 0, C: 0, P: 1, ST_C: 1} 28 sv absolute 2
DEBUG:root:{TICK: 33, IP: 29, ADDR: 2, ACC: 87, Z: 0, C: 0, P: 1, ST_C: 0} 29 ld absolute 6
DEBUG:root:{TICK: 34, IP: 29, ADDR: 6, ACC: 87, Z: 0, C: 0, P: 1, ST_C: 1} 29 ld absolute 6
DEBUG:root:{TICK: 35, IP: 30, ADDR: 6, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 0} 30 sv absolute 2
DEBUG:root:{TICK: 36, IP: 30, ADDR: 2, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 1} 30 sv absolute 2
DEBUG:root:{TICK: 37, IP: 31, ADDR: 2, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 0} 31 ld absolute 10
DEBUG:root:{TICK: 38, IP: 31, ADDR: 10, ACC: 111, Z: 0, C: 0, P: 1, ST_C: 1} 31 ld absolute 10
DEBUG:root:{TICK: 39, IP: 32, ADDR: 10, ACC: 114, Z: 0, C: 0, P: 1, ST_C: 0} 32 sv absolute 2
DEBUG:root:{TICK: 40, IP: 32, ADDR: 2, ACC: 114, Z: 0, C: 0, P: 1, ST_C: 1} 32 sv absolute 2
DEBUG:root:{TICK: 41, IP: 33, ADDR: 2, ACC: 114, Z: 0, C: 0, P: 1, ST_C: 0} 33 ld absolute 5
DEBUG:root:{TICK: 42, IP: 33, ADDR: 5, ACC: 114, Z: 0, C: 0, P: 1, ST_C: 1} 33 ld absolute 5
DEBUG:root:{TICK: 43, IP: 34, ADDR: 5, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 0} 34 sv absolute 2
DEBUG:root:{TICK: 44, IP: 34, ADDR: 2, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 1} 34 sv absolute 2
DEBUG:root:{TICK: 45, IP: 35, ADDR: 2, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 0} 35 ld absolute 11
DEBUG:root:{TICK: 46, IP: 35, ADDR: 11, ACC: 108, Z: 0, C: 0, P: 1, ST_C: 1} 35 ld absolute 11
DEBUG:root:{TICK: 47, IP: 36, ADDR: 11, ACC: 100, Z: 0, C: 0, P: 1, ST_C: 0} 36 sv absolute 2
DEBUG:root:{TICK: 48, IP: 36, ADDR: 2, ACC: 100, Z: 0, C: 0, P: 1, ST_C: 1} 36 sv absolute 2
DEBUG:root:{TICK: 49, IP: 37, ADDR: 2, ACC: 100, Z: 0, C: 0, P: 1, ST_C: 0} 37 ld absolute 12
DEBUG:root:{TICK: 50, IP: 37, ADDR: 12, ACC: 100, Z: 0, C: 0, P: 1, ST_C: 1} 37 ld absolute 12
DEBUG:root:{TICK: 51, IP: 38, ADDR: 12, ACC: 33, Z: 0, C: 0, P: 1, ST_C: 0} 38 sv absolute 2
DEBUG:root:{TICK: 52, IP: 38, ADDR: 2, ACC: 33, Z: 0, C: 0, P: 1, ST_C: 1} 38 sv absolute 2
DEBUG:root:{TICK: 53, IP: 39, ADDR: 2, ACC: 33, Z: 0, C: 0, P: 1, ST_C: 0} 39 halt data 0
INFO:root:output: 72(H)101(e)108(l)108(l)111(o)44(,)32( )87(W)111(o)114(r)108(l)100(d)33(!)
Output:
72(H)101(e)108(l)108(l)111(o)44(,)32( )87(W)111(o)114(r)108(l)100(d)33(!)
instr_counter: 27 ticks: 54
```


CI:

Сделан по образцу преподавателя.


| ФИО           | алг.  | LoC | code байт | code инстр. | инстр. | такт. | вариант |
|---------------|-------|-----|-----------|-------------|--------|-------|---------|
| Бойко В.А.    | hello | 77  | -         | 40          | 27     | 54    | `asm | acc | neum | mc | tick | struct | stream | mem | prob2`     |
| Бойко В.А.    | cat   | 12  | -         | 8           | 15     | 23    | `asm | acc | neum | mc | tick | struct | stream | mem | prob2`     |
| Бойко В.А.    | prob2 | 47  | -         | 25          | 408    | 680   | `asm | acc | neum | mc | tick | struct | stream | mem | prob2`     |

